from PyQt5 import QtCore, QtGui, QtWidgets, uic
#from PA_functions import *
import threading
import sympy
import math
from scipy.optimize import fsolve, leastsq
import time
import matplotlib.pyplot as plt
import numpy as np
import re
import pyqtgraph
from sympy.integrals.transforms import inverse_laplace_transform

class MyWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(MyWindow, self).__init__()
        uic.loadUi('OMg.ui', self)
        self.calc_PB.clicked.connect(self.calc)
        self.msg_font = 'Cantarell'

        self.setFixedSize(self.size())

    def solver(self, z, c):
        F = list()
        for i in range(len(z)):
            F.append(0)
            F[i] = self.eq[i]
            for j in range(len(z)):
                F[i] = F[i].subs(c[j], z[j])
        return F

    def __check_input__(self, input, input_name):
        """
        Type: private
        :param input: correct input must consist of any sequence of numbers separated by whitespaces (each single number separated by single whitespace)
        :param input_name: either 'Nominator' or 'Denominator'
        :return: list of numbers if input is correct, None otherwise
        """
        regex_check = re.match('((([\d]+.[\d]*\s)|([\d]+\s))+)*(([\d]+.[\d]*(\s{1})?)|[\d]+(\s{1})?)', input)
        def err_msg():
            self.message(input_name + ' must contain series of floats separated by whitespaces\nExample:\n8.99 5.678 3.45',
                         'Wrong input', 'ERROR', self.msg_font)
        # if checked input does not match given Regex-string throw an error
        if regex_check is None or input != regex_check.string[regex_check.start():regex_check.end()]:
            err_msg()
            return
        else:
            # string is converted into a list, whitespaces are removed
            result = [i for i in input.split(' ') if i != '']
            # Additional check since Python-Regex works incorrectly if to type in some alphanumeric symbols like 1.2 3 4U
            # although Regex-string is strictly defined to contain only integer or floats with whitespaces
            for i in result:
                try:
                    temp = float(i)
                except ValueError:
                    err_msg()
                    return
        return result

    def calc(self):
        start_time = time.time()

        nominator = self.__check_input__(self.nominator_LE.text(), 'Nominator')
        denominator = self.__check_input__(self.denominator_LE.text(), 'Denominator')

        # Interpreting values as symbolic
        L, r, R, t, s, self.Der, self.Pro, self.Int = sympy.S('L, r, R, t, s, D, K, TI')

        n = sympy.symbols('nominator')
        n /= n
        for i in nominator:
            n *= 1+float(i)*r

        d = sympy.symbols('denominator')
        d /= d
        for i in denominator:
            d *= 1+float(i)*r

        obj = n/d

        # Delay
        if self.delay_LE.text() == '':
            delay = 1
        else:
            delay = self.isFloat(self.delay_LE.text(), 'Delay must be float')
            obj *= math.e**(-r)

        obj = obj.subs(r, s*delay)
        print(obj)
        print(inverse_laplace_transform(obj, s, t))

        R = self.Pro
        c = [self.Pro]
        if self.p_reg_RB.isChecked():
            self.length = 1
        elif self.pi_reg_RB.isChecked():
            c.append(self.Int)
            self.length = 2
            R *= (r + 1/self.Int)
            d *= r
        elif self.pd_reg_RB.isChecked():
            c.append(self.Der)
            self.length = 2
            R *= (1+self.Der*r)
        elif self.pid_reg_RB.isChecked():
            c.append(self.Int)
            c.append(self.Der)
            self.length = 3
            R *= (1/self.Int+self.Der*r*r+r)
            d *= r

        L = R*n
        if self.delay_LE.text() != '':
            d *= (math.e ** r)
        d += L

        self.eq = list()
        nom = 0
        denom = 0
        m = 0
        Cmk = 0
        for i in range(self.length):
            m = 2*(i+1)
            nom = 0
            denom = 0
            for k in range(m+1):
                Cmk = math.factorial(m)/(math.factorial(m-k)*math.factorial(k))
                nom += Cmk*sympy.diff(L, r, m-k)*sympy.diff(L, r, k)*(-1)**k
                denom += Cmk*sympy.diff(d, r, m-k)*sympy.diff(d, r, k)*(-1)**k
            self.eq.append(nom-denom)

        init = list()
        for i in range(self.length):
            self.eq[i] = self.eq[i].subs(r, 0)
            init.append(5)

        solution_f = fsolve(self.solver, init, c)
        solution_sq = leastsq(self.solver, init, c)

        # Controller settings derived by 'Fsolve'
        self.prop_f_LE.setText(str(round(solution_f[0], 3)))
        try:
            self.int_f_LE.setText(str(round(solution_f[1], 3)))
        except IndexError:
            self.int_f_LE.setText('')
        try:
            self.der_f_LE.setText(str(round(solution_f[2], 3)))
        except IndexError:
            self.der_f_LE.setText('')

        # Controller settings derived by 'Least Squares'
        self.prop_sq_LE.setText(str(round(solution_sq[0][0], 3)))
        try:
            self.int_sq_LE.setText(str(round(solution_sq[0][1], 3)))
        except IndexError:
            self.int_sq_LE.setText('')
        try:
            self.der_sq_LE.setText(str(round(solution_sq[0][2], 3)))
        except IndexError:
            self.der_sq_LE.setText('')

        # self.graphWidget = pyqtgraph.PlotWidget()
        # self.setCentralWidget(self.graphWidget)
        #
        # hour = [1,2,3,4,5,6,7,8,9,10]
        # temperature = [30,32,34,32,33,31,29,32,35,45]
        # self.graphWidget.setBackground('w')
        # # plot data: x, y values
        # self.graphWidget.plot(hour, temperature)

        self.time_LE.setText("{0}".format(round(float(time.time() - start_time), 3)))

        del n, d, L, r, R, self.Der, self.Pro, self.Int, delay, nom, denom, solution_f, solution_sq, Cmk, self.eq, c

    def isFloat(self, input, msg):
        try:
            input = float(input)
        except ValueError:
            self.message(msg, 'Wrong input', 'ERROR', self.msg_font)
        return input

    def message(self, msg, title, flag, msg_font):
        messageBox = QtWidgets.QMessageBox()
        messageBox.setText(msg)
        messageBox.setWindowTitle(title)
        if flag == 'OK':
            messageBox.setStyleSheet("QMessageBox { background-color: green; font: bold 14pt " + msg_font + ";}")
        elif flag == 'ERROR':
            messageBox.setStyleSheet("QMessageBox { background-color: red; font: bold 14pt " + msg_font + ";}")
        messageBox.exec_()

if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())